<?php
require 'vendor/autoload.php'; // Make sure to adjust the path if needed

use MysqliDb;

$host = 'localhost';
$username = 'root';
$password = '';
$databaseName = 'employee';

$db = new MysqliDb($host, $username, $password, $databaseName);

// Create (Insert) Operation
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['create'])) {
    $first_name = $_POST['first_name'];
    $last_name = explode(",", $_POST['last_name']);
    $middle_name = $_POST['middle_name'];
    $birthday = $_POST['birthday'];
    $address = $_POST['address'];

    $data = [
        'first_name' => $first_name,
        'last_name' => $last_name,
        'middle_name' => $middle_name,
        'birthday' => $birthday,
        'address' => $address
    ];

    if ($db->insert('employee', $data)) {
        echo "New record created successfully";
    } else {
        echo "Error: " . $db->getLastError();
    }
}

// Read Operation
$employees = $db->get('employee');

// Update Operation
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['update'])) {
    $update_id = $_POST['update_id'];
    $new_address = $_POST['new_address'];

    $data = ['address' => $new_address];
    $db->where('id', $update_id);
    if ($db->update('employee', $data)) {
        echo "Record updated successfully";
    } else {
        echo "Error updating record: " . $db->getLastError();
    }
}

// Delete Operation
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['delete'])) {
    $delete_id = $_POST['delete_id'];

    $db->where('id', $delete_id);
    if ($db->delete('employee')) {
        echo "Record deleted successfully";
    } else {
        echo "Error deleting record: " . $db->getLastError();
    }
}
?>